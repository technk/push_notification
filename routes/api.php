<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::post('register', 'Api\Apicontroller@create')->middleware('token');
Route::post('login', 'Api\Apicontroller@login')->middleware('token');
Route::post('get_value', 'Api\Apicontroller@get_value')->middleware('token');

Route::get('/logout', function () {
    return view('logout');
});




