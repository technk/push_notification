<?php

namespace App\Http\Middleware;

use App\Model\Apitoken;
use App\User;
use Closure;
use Illuminate\Support\Str;

class ForeJsonToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if ($request->input('token')) {
            $user = User::where('token', $request->input('token'))->first();
            if ($user) {
                $response = ['logout' => 'Your can`t Login'];
                $Apitoken = Apitoken::where('user_id', $user->id)->orderby('date','DESC')->first();
                if (!empty($Apitoken)) {
                    $t1 = strtotime(date('h:i', strtotime($Apitoken->date)));
                    $t2 = strtotime(date('h:i'));
                    $expery = date("H:i", strtotime('+20 minutes', $t1)); // log out time
                    $total_sec = abs($t2 - $t1);
                    $total_min = floor($total_sec / 60);
                    $total_hour = floor($total_min / 60);
                    $total_day = floor($total_hour / 24);
                    $hour = $total_hour - ($total_day * 24);
                    $min = $total_min - ($total_hour * 60);
                    if ((int) number_format($min) == 20) {
                        $token = uniqid(base64_encode(Str::random(60)));
                        $response = ['token' => $token];
                        $curret_user = User::where('id', '=', $user->id)->first();
                        $api_request = Apitoken::find($Apitoken->id);
                        $api_request->user_id = $curret_user->id;
                        $api_request->date = date('Y-m-d h:i:s');
                        $api_request->created_by = $curret_user->id;
                        $api_request->save();
                        $user = User::where('id', '=', $user->id)->update($response);
                        $result = $request;
                        $result['token'] = $token;
                        // NOTE New token will generate and it will send to controller page & store to user particular person.
                        $res = $next($result);
                    } else if ((int) number_format($min) > 21) {
                        // NOTE Token will expiry and it can be logout here.
                        $res = redirect('/logout');
                    } else {
                        // NOTE Request can send controller page here.
                        $res = $next($request);
                    }
                } else {
                    // NOTE Specify log file cannot find them here.
                    $res = $next($request);
                }
            } else {
                // NOTE Not user request cannot find them here.
                $res = redirect(route('/login'));
            }
        } else {
            // NOTE login person request here.
            $res = $next($request);
        }
        return $res;
    }
}
