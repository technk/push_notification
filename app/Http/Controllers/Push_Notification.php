<?php
namespace App\Http\Controllers;
use FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Sly\NotificationPusher\PushManager;
use Sly\NotificationPusher\Adapter\Apns as ApnsAdapter;
use PushNotification;
class Push_Notification extends Controller
{
    //
    public function index()
    {
        try {
            $device_token = "fdcSCdAB5WY:APA91bHNAZsWmYNeqn6-ZgBjdzdwhnteEbF2wYsP8-ivUEEP0MoFYFVa5oLA_YtlY8BwTu60wkd_DTIRSr3xYAu44ZDx4NvIDpvHjhfzlxTPMWLoCHumrsVyWb5ZzPqbjnM_NXEX5bW2";
            // get a token from mobile phone
            $message = PushNotification::Message('Message Text 1', array(
                'badge' => 1,
                'sound' => 'example.aiff',
                'actionLocKey' => 'Action button title!',
                'locKey' => 'localized key',
                'locArgs' => array(
                    'localized args',
                    'localized args',
                ),
                'launchImage' => 'image.jpg',
                'custom' => array('custom data' => array(
                    'we' => 'want', 'send to app',
                )),
            ));
            $push = PushNotification::app('appNameAndroid') // constant do not change here . because Message cannot sent it. # go to file path "config/push-notification.php" .
                ->to($device_token)
                ->send($message);
            $push->adapter->setAdapterParameters(['sslverifypeer' => false]);
            $res = (Object) array('status' => 'Success', 'message' => 'Message send it !.');
        } catch (\Exception $th) {
            $res = (Object) array('status' => 'error', 'message' => $th->getMessage());
        }
        return json_encode([$res]);
    }
    public function test_notify()
    {
        try {
            //TODO get link https: //github.com/brozot/Laravel-FCM
            $token = "fdcSCdAB5WY:APA91bHNAZsWmYNeqn6-ZgBjdzdwhnteEbF2wYsP8-ivUEEP0MoFYFVa5oLA_YtlY8BwTu60wkd_DTIRSr3xYAu44ZDx4NvIDpvHjhfzlxTPMWLoCHumrsVyWb5ZzPqbjnM_NXEX5bW2";
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);
            $notificationBuilder = new PayloadNotificationBuilder('Rapro');
            $notificationBuilder->setBody('Hello world')
                ->setSound('default');
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(['a_data' => 'my_data']);
            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            // $token = ""; // get a token from mobile phone
            $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $res = (Object) array('Failure' => $downstreamResponse->numberFailure(), 'Success' => $downstreamResponse->numberSuccess(), 'message' => 'Message sends it !.');
        } catch (\Exception $th) {
            //throw $th;
            $res = (Object) array('status' => 'error', 'message' => $th->getMessage());
        }
        return json_encode([$res]);
    }
    public function test()
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token = "e6t_bhHjOys:APA91bHB-dp663PR0Y4J1o1K5v8wsv7pkp6RAY9spJbLPEZpqDZiCDIGBqF8w2cd_IyupHucV-1bBZzyabwIkEzSkxBd9EaopE_2EYWukq91OeJ-rhuwmnTh7jiIJhPb71uB3R1GOhkP";
        $notification = [
            'body' => 'this is test',
            'sound' => true,
        ];
        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData,
        ];
        $headers = [
            'Authorization: key=AIzaSyDAGmffvZ-52tB_1YSEV0dFswbbFrylTgY', // firebase Legacy server key
            'Content-Type: application/json',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        dd($result);
    }
    public function get_folder()
    {
        $base_path = base_path();
        $pieces = explode("\\", $base_path);
        array_pop($pieces);
        // C:\xampp\htdocs
        $htdocs_folder_path = implode("\\", $pieces);
        $dir = $htdocs_folder_path;
        //In server scandir($dir) shows error, so we put condition
        if (!file_exists($dir) && !is_dir($dir)) {
            return false;
        }
        $ffs = scandir($dir);
        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);
        // prevent empty ordered elements
        if (count($ffs) < 1) {
            return;
        }
        foreach ($ffs as $ff) {
            if (is_dir($dir . '/' . $ff)) {
                $array[] = $ff;
            }
        }
        $htdocs_folders = $array;
        foreach ($htdocs_folders as $key => $value) {
            $dir = $htdocs_folder_path . "\\" . $value;
            $ffs = scandir($dir);
            unset($ffs[array_search('.', $ffs, true)]);
            unset($ffs[array_search('..', $ffs, true)]);
            foreach ($ffs as $ff) {
                if (is_dir($dir . '/' . $ff)) {
                    if ($ff == "app") {
                        $dir = str_replace($htdocs_folder_path . '\\', '', $dir);
                        $filtered_laravel_folder_array[] = $dir;
                        break;
                    }
                }
            }
        }
        $route_filename = base_path() . "\\routes\api.php";
        if (file_exists($route_filename)) {
            $api_data = 'Route::resource(\'' . 'hello' . '\',\'' . '$this->controller_folder_name' . '\\' . '$this->capitalized_controller_name' . '\');';
            $new_line_data = PHP_EOL . $api_data;
            $all_route_contents = file_get_contents($route_filename);
            if (strpos($all_route_contents, $api_data)) {
                echo "have already.";
            } else {
                file_put_contents($route_filename, $new_line_data, FILE_APPEND);
                echo "API created Successfully..";
            }
        }
        dd($route_filename = app_path() . "\\Http\Controllers\push.php");
    }
}
