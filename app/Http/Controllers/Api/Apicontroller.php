<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Apitoken;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Auth;
class Apicontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('token');
    }

    public function login_page()
    {
        return view('welcome');

    }

    protected function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $input = $request;
        $input['password'] = Hash::make($request->input('password'));
        $input['token'] = Str::random(60);

        User::create($input->toArray());

        return response($request, 200);

    }

    protected function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password])) {
                $token = uniqid(base64_encode(Str::random(60)));
                $response = ['token' => $token, 'access_time' => 1];
                $curret_user = User::where('id', '=', $user->id)->first();
                $user = User::where('id', '=', $user->id)->update($response);
                $input = [
                    'user_id' => $curret_user->id,
                    'scopes' => 'Login',
                    'date' => date('Y-m-d h:i:s'),
                    'created_by' => $curret_user->id,
                ];

                $data=Apitoken::where('user_id',$curret_user->id)->first();

               if(!empty($data)){
                        $api_request = Apitoken::find($data->id);
                        $api_request->user_id = $curret_user->id;
                        $api_request->date = date('Y-m-d h:i:s');
                        $api_request->created_by = $curret_user->id;
                        $api_request->save();
                }else{
                    Apitoken::insert($input);
                }
                return view('welcome', $response);
            } else {
                $response = "Password missmatch";
                return response($response, 422);
            }
        } else {
            $response = 'User does not exist';
            return response($response, 422);
        }
    }

    public function get_value(Request $request)
    {

        $user = User::where('email', $request->email)->first();

        return json_encode($user);
    }


public function logina(Request $request) {

            if (Auth::attempt ( array ('name' => $request->get ( 'username' ),'password' => $request->get ( 'password' )) )) {
                session ( ['name' => $request->get ( 'username' ) ] );
                return Redirect::back ();
            } else {
                Session::flash ( 'message', "Invalid Credentials , Please try again." );
                return Redirect::back ();
            }
}


}
