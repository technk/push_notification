<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class message_content extends Model
{
    //
    protected $fillable = [
        'feature',
        'process',
        'contents',
        'content_variables',
        'status',
    ];
}
