<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Apitoken;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request)
    {
        return $request->all();
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $current_id = Auth::id();
                $token = uniqid(base64_encode(Str::random(60)));
                $response = ['token' => $token, 'access_time' => 1];
                $curret_user = User::where('id', '=', $current_id)->first();
                $user = User::where('id', '=', $current_id)->update($response);
                $input = [
                    'user_id' => $current_id,
                    'scopes' => 'Login',
                    'date' => date('Y-m-d h:i:s'),
                    'created_by' => $current_id,
                ];

                $data=Apitoken::where('user_id',$current_id)->first();

               if(!empty($data)){
                        $api_request = Apitoken::find($data->id);
                        $api_request->user_id = $current_id;
                        $api_request->date = date('Y-m-d h:i:s');
                        $api_request->created_by = $current_id;
                        $api_request->save();
                }else{
                    Apitoken::insert($input);
                }
                return redirect()->intended('home');

        }else {
            $response = 'User does not exist';
            return response($response, 422);
        }
    }

}
