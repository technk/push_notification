<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AAAAl6ZxKP0:APA91bErOwk1hQdECYNu_8BzalP-M6SC0oqG0AWfMVBNuHT8e-qbcJZIUouFOufpTC4O3Z8a5CnyztOeU_GGDdzaglNSgsIS62erSSWed0i781yqJsvWMBgMg9nj1zPhC8muKHv1EzSX', // firebase apikey
        'service'     =>'gcm'
    )

);
