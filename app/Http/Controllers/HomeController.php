<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\message_content;
use Illuminate\Support\Facades\Validator;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function get_content_variable(){

        $arrayName = array(
            '1' =>  'current_date',
            '2' =>  'current_user_name',
            '3' =>  'current_user_id',
            '4' =>  'current_user_email',
            '5' =>  'receiver_user_name',
            '6' =>  'receiver_user_id',
            '7' =>  'receiver_user_email',
            '8' =>  'password_pwd',
        );

        $result = array('status' =>'success' ,'data'=> $arrayName);

        return $result ;
    }


    public function get_message_content_store(Request $request){

       $id=$request->input('id');
       $content_features=$request->input('content_features');
       $content_process=$request->input('content_process');
       $content_title=$request->input('content_title');
       $content_subject=$request->input('content_subject');
       $content_variables=$request->input('content_variables');
       $source=$request->input('source');
       $sender_push_message_content=$request->input('sender_push_message_content');
       $sender_email_message_content=$request->input('sender_email_message_content');
       $reciver_push_message_content=$request->input('reciver_push_message_content');
       $reciver_email_message_content=$request->input('reciver_email_message_content');

    $validator = Validator::make($request->all(), [
        'content_features' => 'required',
        'content_process' => 'required',
        'content_title' => 'required',
        'content_subject' => 'required',
        'source' => 'required',
        'push_message_content' => 'required',
        'email_message_content' => 'required',
    ]);


       if ($validator->fails()) {
            $data = array('status' => 'Error', 'message' => 'Validation Failed', 'errors' => $validator->errors());
        } else {

       $content = (object)array(
        'content_title'=>$content_title,
        'content_subject'=>$content_subject,
        'source'=>$source,
        'push_message_content'=>$push_message_content,
        'email_message_content'=>$email_message_content,
       );

        $store = array(
        'feature'=>$content_features,
        'process'=>$content_process,
        'contents'=>json_encode($content),
        'content_variables'=>json_encode($content_variables),
        'status'=>1,
       );


       if(!empty($id)){
          $data = message_content::find($id)->update($store);
        } else{
          $data = message_content::create($store);
        }

        $data = array('status' => 'Success', 'message' =>'Success');

    }
            return $data;

    }

    public function send()
    {
        # code...
        $sender=2;
        $reciver = [1];
        $pwd = 123456;

        // return $this->get_mesage($sender,$reciver,$pwd);
    }


    public function get_mesage($sender_id,$reciver_id,$pwd)
    {
       $reciver_list=User::whereIn('id',$reciver_id)->get();

              foreach ($reciver_list as $value) {
                    $data = message_content::where('id','4')->first();
                    $contents=json_decode($data->contents);
                    $content_variables=json_decode($data->content_variables);

                    $sender = User::where('id', $sender_id)->first();

                    // content variable value assign here

                    // sender as current user and sending some information about details .
                    $current_user_name = $sender->name;
                    $current_user_id = $sender->user_id;
                    $current_user_email = $sender->email;

                    // receiver details some information about here .
                    $receiver_user_name = $value->name;
                    $receiver_user_id = $value->user_id;
                    $receiver_user_email = $value->email;
                    $password_pwd = $pwd;

                    foreach ($content_variables as  $variable) {
                            try {
                                $contents->push_message_content = str_replace($variable, $$variable, $contents->push_message_content);
                            } catch (\Throwable $th) {
                                //   dd($th->getMessage());
                            }
                    }



              }

            return response($reciver_list,200);

    }




}
