
var app = angular.module('myApp', []);

app.factory('MasterLayout_Services', function ($http) {
    return {
        get_list: function (url) {
            return $http.get(url).then(function (data) {
                return data.data;
            });
        },
        post_list: function (url, obj) {
            return $http.post(url, obj).then(function (data) {
                return data.data;
            });
        }
    };
});


app.controller('myCtrl', function ($scope, $http, MasterLayout_Services) {
    // $scope.firstName = "John";
    // $scope.lastName = "Doe";
    // alert('hello');

    $scope.data = {};

    $scope.send_source_details = [
        {
         'msg_reciver':'',
         'msg_for':[],
         'content_title':'',
         'content_subject':'',
         'email_message_content':'',
         'push_message_content':'',
        }];


    $scope.addNew = function (ltem) {
        $scope.send_source_details.push({
            'msg_reciver': '',
            'msg_for': [],
            'content_title': '',
            'content_subject': '',
            'email_message_content': '',
            'push_message_content': '',
        });
        console.log($scope.send_source_details);

    };
    $scope.removeElement = function (ltem,index) {
        $scope.send_source_details.splice(index,1)
        console.log($scope.send_source_details,index);
    };



    $scope.intial=function(){
        MasterLayout_Services.get_list("get_content_variable")
        .then(function (response) {
            $scope.content_variables = response.data;
        });
    }
    $scope.intial();

    $scope.space=function(data,status){

        if (data.email_message_content != undefined){
            data.email_message_content = data.email_message_content + '   ';
        }
    }

    // $scope.msg_for=[];

    $scope.send_source = function (index,ltem,source){

        $scope.index=$scope.msg_for.indexOf(source);
        if ($scope.index == -1){
            $scope.msg_for.push(source);
        }else{
            $scope.msg_for.splice($scope.index);
        }
    }

    $scope.msg_reciver_store = function (index, ltem,source){
        console.log(index, ltem,source);
    }



    $scope.get_word_change_email= function(data){
        console.log(data.email_message_content + ' ' + data.email_content_variable);
        if (data.email_message_content != undefined){
            data.email_message_content = data.email_message_content + ' ' + data.email_content_variable + ' ';
        }
    }

    $scope.get_word_change_push = function (data) {
        console.log(data.push_message_content + ' ' + data.push_content_variable);
        if (data.push_message_content != undefined) {
            data.push_message_content = data.push_message_content + ' ' + data.push_content_variable + ' ';
        }
    }

        $scope.submit = function (data) {
            data.source = $scope.msg_for;
            data.content_variables=$scope.content_variables ;
            MasterLayout_Services.post_list("get_message_content_store",data)
                .then(function (response) {
                    // $scope.content_variables = response.data;
                    if (response.status == "Error"){
                        $scope.errors = response.errors;
                    }else{
                        $scope.message = response.message;
                        $scope.data = {};
                    }
                    console.log(response);

                });
        }

        $scope.reset = function () {
           $scope.data={};
        }


});
