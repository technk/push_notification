<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Auth::routes();

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', function () {
    return redirect(route('login'));
});

Route::post('/custom_login', 'Auth\LoginController@authenticate')->middleware(['Execption']);
Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth', 'token']);

Route::get('/logout', function () {
    return redirect(route('logout'));
});

Route::group(['middleware' => ['token', 'Execption']], function () {

    Route::post('register', 'Api\Apicontroller@create');
    Route::post('get_value', 'Api\Apicontroller@get_value');

    Route::get('/get_content_variable', 'HomeController@get_content_variable')->name('get_content_variable');
    Route::post('/get_message_content_store', 'HomeController@get_message_content_store')->name('get_message_content_store');
    Route::get('/get_mesage', 'HomeController@get_mesage')->name('get_mesage');

    // Route::post('/send', 'Push_Notification@index')->middleware('Execption');

});
Route::get('/send', 'Push_Notification@index');

// Route::get('/timeout', 'Push_Notification@timeout')->middleware('Execption');