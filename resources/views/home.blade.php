@extends('layouts.app')

@section('content')
<section ng-controller="myCtrl">


    <div class="container p-3 mb-5 rounded "
        style="background: #e4e2e296; box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.5)!important;">
        <form>
            <!-- pick one of these 2 -->
            {!! csrf_field() !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="row ">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                    <div class="form-group">
                        <label for="">Features</label>
                        <input type="text" class="form-control" ng-model="data.content_features" placeholder="Features">
                    </div>

                    <div class="alert alert-danger" ng-if="errors">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <span ng-bind="errors.content_features[0]"></span>
                    </div>



                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                    <div class="form-group">
                        <label for="">Process</label>
                        <input type="text" class="form-control" ng-model="data.content_process" placeholder="Process">
                    </div>
                    <div class="alert alert-danger" ng-if="errors">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <span ng-bind="errors.content_process[0]"></span>
                    </div>
                </div>

            </div>


            <div ng-repeat="ltem in send_source_details">
                <div class="row ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <label for="">Message For </label>
                        <div class="container">
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio"
                                            ng-click="msg_reciver_store($index,ltem,'sender')"
                                            ng-model="ltem.msg_reciver" name="sender" value="sender"> Sender
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio"
                                            ng-click="msg_reciver_store($index,ltem,'receiver')"
                                            ng-model="ltem.msg_reciver" name="receiver" value="receiver"> Receiver
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger" ng-if="errors">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span ng-bind="errors.source[0]"></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <label for="">Send Source</label>
                        <div class="container">
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"
                                            ng-click="send_source($index,ltem,'email')" ng-model="ltem.email"
                                            name="email" value="email"> Email
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"
                                            ng-click="send_source($index,ltem,'push')" ng-model="ltem.push" name="push"
                                            value="push"> Push
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"
                                            ng-click="send_source($index,ltem,'sms')" ng-model="ltem.sms" name="sms"
                                            value="sms"> SMS
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger" ng-if="errors">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span ng-bind="errors.source[0]"></span>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input class="form-control" placeholder="Title" ng-model="ltem.content_title">
                        </div>
                        <div class="alert alert-danger" ng-if="errors">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span ng-bind="errors.content_title[0]"></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <div class="form-group">
                            <label for="">Subject</label>
                            <input type="text" class="form-control" ng-model="ltem.content_subject"
                                placeholder="Subject">
                        </div>
                        <div class="alert alert-danger" ng-if="errors">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span ng-bind="errors.content_subject[0]"></span>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <div class="form-group">
                            <label for="">Email Mesage Content</label> <span class="badge badge-primary"
                                ng-click="space($index,ltem,'space')">Space</span>
                            <textarea class="form-control" placeholder="Email Mesage Content"
                                ng-model="ltem.email_message_content" rows="3"></textarea>
                        </div>
                        <div class="alert alert-danger" ng-if="errors">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span ng-bind="errors.email_message_content[0]"></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <div class="form-group">
                            <label for="">Content Variable</label>
                            <select class="custom-select" ng-model="ltem.email_content_variable"
                                ng-change="get_word_change_email($index,ltem)">
                                <option value="" selected>--Select One--</option>
                                <option ng-repeat="item in content_variables" value="@{{item}}">@{{item}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <div class="form-group">
                            <label for="">Push Mesage Content</label>
                            <textarea class="form-control" placeholder="Push Mesage Content"
                                ng-model="ltem.push_message_content" rows="3"></textarea>
                        </div>
                        <div class="alert alert-danger" ng-if="errors">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span ng-bind="errors.push_message_content[0]"></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                        <div class="form-group">
                            <label for="">Content Variable</label>
                            <select class="custom-select" ng-model="ltem.push_content_variable"
                                ng-change="get_word_change_push($index,ltem)">
                                <option value="" selected>--Select One--</option>
                                <option ng-repeat="item in content_variables" value="@{{item}}">@{{item}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="float-right ml-3">
                    <button type="button" class="btn btn-danger" ng-click="removeElement(ltem,$index)">Remove</button>
                </div>
            </div>
            <div class="float-right">
                <button type="button" class="mx-auto btn btn-primary" ng-click="addNew(ltem)">Add</button>
            </div>
            <div class="row justify-content-center">
                <button type="button" class="mx-auto btn btn-primary" ng-click="submit(data)">Submit</button>
                <button type="button" class="mx-auto btn btn-success" ng-click="reset()">Clear</button>
            </div>
        </form>
</section>
@endsection