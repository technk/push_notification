@extends('layouts.app')

@section('content')
  <section  ng-controller="myCtrl">


    <div class="container p-3 mb-5 rounded " style="background: #e4e2e296; box-shadow: 0 0.5rem 1rem rgba(0,0,0,0.5)!important;">
    <form >
    <!-- pick one of these 2 -->
    {!! csrf_field() !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="row ">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Features</label>
                    <input type="text" class="form-control" ng-model="data.content_features"  placeholder="Features">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Process</label>
                    <input type="text" class="form-control" ng-model="data.content_process"  placeholder="Process">
                </div>
            </div>
        </div>
         <div class="row ">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Title</label>
                    <input class="form-control" placeholder="Title" ng-model="data.content_title" >
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Subject</label>
                    <input type="text" class="form-control" ng-model="data.content_subject"  placeholder="Subject">
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Email Mesage Content</label> <span class="badge badge-primary" ng-click="space(data,'space')">Space</span>
                    <textarea class="form-control" placeholder="Email Mesage Content" ng-model="data.email_message_content"  rows="3"></textarea>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Content Variable</label>
                    <select class="custom-select" ng-model="data.email_content_variable"  ng-change="get_word_change_email(data)">
                        <option value="" selected >--Select One--</option>
                        <option ng-repeat="item in content_variables" value="@{{item}}">@{{item}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Push Mesage Content</label>
                    <textarea class="form-control" placeholder="Push Mesage Content" ng-model="data.push_message_content"  rows="3"></textarea>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 mt-5">
                <div class="form-group">
                    <label for="">Content Variable</label>
                    <select class="custom-select" ng-model="data.push_content_variable"  ng-change="get_word_change_push(data)">
                        <option value="" selected >--Select One--</option>
                        <option ng-repeat="item in content_variables" value="@{{item}}">@{{item}}</option>
                    </select>
                </div>
            </div>
        </div>
    <div class="row justify-content-center">
            <button type="button" class="mx-auto btn btn-primary" ng-click="submit(data)">Submit</button>
            <button type="button" class="mx-auto btn btn-success" ng-click="reset()">Clear</button>
    </div>
</form>
  </section>
@endsection
