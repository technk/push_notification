<?php

namespace App\Http\Middleware;

use Closure;

class ExecptionHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $output = $next($request);
// dd($output,'Exception');
        try {

            if (!is_null($output->exception)) {

                date_default_timezone_set('Asia/Kolkata');
                // $error=array(json_encode(['date' => date('d-m-Y h:i:s'),'Message' => $output->exception->getMessage(), 'line' => $output->exception->getline(), 'code' => $output->exception->getcode(),'file' =>$output->exception->getfile()], 500));

                $error = "date = " . date('d-m-Y h:i:s') . " ," . "Message = " . $output->exception->getMessage() . " ," . "line = " . $output->exception->getline() . " ," . " code = " . $output->exception->getcode() . " ," . "file = " . $output->exception->getfile() . PHP_EOL;

                $destinationPath = base_path() . "\\public\Exception\generate_json";
                $json_filepath_with_name = $destinationPath . "\\" . date('Y-m-d') . ".json";

                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }

                \File::append($json_filepath_with_name, json_encode($error) . ',' . PHP_EOL);
                return response()->json(['Message' => $output->exception->getMessage(), 'line' => $output->exception->getline(), 'code' => $output->exception->getcode(), 'file' => $output->exception->getfile()], 500);
            }

        } catch (\TooManyRequestsHttpException $e) {
            return response()->json('this string is never showed up', 429);
        } catch (\ValidationException $e) {
            return response()->json('validation error', 400);
        } catch (\ModelNotFoundException $e) {
            return response()->json('not found', 404);
        } catch (\Exception $e) {
            return response()->json($error, 500);
        }
        return $output;
    }
}